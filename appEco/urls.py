from django.urls import path, include
from .views import *

from rest_framework import routers

router = routers.DefaultRouter()
router.register('actividades', ActividadViewSet)

urlpatterns = [
    path('', home, name="home"),
    path('galeria/', galeria, name="galeria"),
    path('listado-actividades/', listado_actividades, name="listado_actividades"),
    path('agregar-actividad/', agregar_actividad, name="agregar_actividad"),
    path('editar-actividad/<id>/', editar_actividad, name="editar_actividad"),
    path('eliminar-actividad/<id>/', eliminar_actividad, name="eliminar_actividad"),
    path('inscribir-usuario/<id>/', inscribir_usuario, name="inscribir_usuario"),
    path('cancelar-usuario/<id>/', cancelar_usuario, name="cancelar_usuario"),
    path('perfil-usuario/', perfil_usuario, name="perfil"),
    path('basic/', include(router.urls )),
]