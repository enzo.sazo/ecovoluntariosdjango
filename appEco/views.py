from django.shortcuts import render, redirect
from .models import Actividad, Perfil
from .forms import ActividadForm
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User

# Rest Framework
from rest_framework import viewsets
from .serializers import ActividadSerializer 

# Create your views here.


def home(request):
    return render(request, 'appEco/home.html')

def galeria(request):
    data = {
        'actividades':Actividad.objects.all()
    }
    return render(request, 'appEco/galeria.html', data)

#función que permite filtrar el listado de actividades
def listado_actividades(request):
    titulo = request.GET.get("titulo")
    tipo = request.GET.get("tipo")
    actividades = []


    if 'btnTipo' in request.GET:
        if tipo:
            actividades = Actividad.objects.filter(
                tipo = tipo
            )
    elif 'btnTitulo' in request.GET:
        if titulo:
            actividades = Actividad.objects.filter(
                titulo__icontains = titulo
            )
    else:
        actividades = Actividad.objects.all()
    

    usuario = request.user
    perfil = Perfil.objects.get(user_id=usuario.id)
    
    actividades_usuario = perfil.actividad.all()

    data = {
        'actividades':actividades,
        'actividades_usuario': actividades_usuario
    }

    return render(request, 'appEco/listado_actividades.html', data)

#función que permite que sólo los usuarios que tengan el permiso add_actividad puedan agregar actividades
@permission_required('appEco.add_actividad')
def agregar_actividad(request):

    data = {
        'form':ActividadForm()
    }

    if request.method == "POST":
        formulario = ActividadForm(request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Guardado Correctamente"
        
    return render(request, 'appEco/agregar_actividad.html', data)

#función que permite que sólo los usuarios que tengan el permiso change_actividad puedan editar actividades
@permission_required('appEco.change_actividad')
def editar_actividad(request, id):
    actividad = Actividad.objects.get(id=id)
    data = {
        'form':ActividadForm(instance=actividad)
    }

    if request.method == "POST":
        formulario = ActividadForm(data=request.POST, instance=actividad, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Editado Correctamente"
        data['form'] = ActividadForm(instance=Actividad.objects.get(id=id))

    return render(request, 'appEco/editar_actividad.html', data)

#función que permite que sólo los usuarios que tengan el permiso delete_actividad puedan eliminar actividades
@permission_required('appEco.delete_actividad')
def eliminar_actividad(request, id):
    actividad = Actividad.objects.get(id=id)
    actividad.delete()

    return redirect(to="listado_actividades")


def perfil_usuario(request):
    usuario = request.user
    perfil = Perfil.objects.get(user_id=usuario.id)
    data = {
        'usuario': usuario,
        'perfil': perfil
    }
    return render(request, 'appEco/perfil_usuario.html', data)

#función que permite relacionar un usuario con las actividades a las que asistirá
def inscribir_usuario(request, id):
    usuario = request.user
    perfil = Perfil.objects.get(user_id=usuario.id)
    

    act = Actividad.objects.get(id=id)
    perfil.actividad.add(act)

    perfil.save()
    
    return redirect(to="listado_actividades")

#función que remueve la relación entre una actividad y el usuario, ya que éste canceló su asistencia
def cancelar_usuario(request, id):
    usuario = request.user
    perfil = Perfil.objects.get(user_id=usuario.id)
    

    act = Actividad.objects.get(id=id)
    perfil.actividad.remove(act)

    perfil.save()
    
    return redirect(to="listado_actividades")


class ActividadViewSet(viewsets.ModelViewSet):
    queryset = Actividad.objects.all()
    serializer_class = ActividadSerializer