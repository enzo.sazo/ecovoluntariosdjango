from django.contrib import admin
from .models import Actividad, Perfil

# Register your models here.
admin.site.register(Actividad)
admin.site.register(Perfil)