from django.db import models
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User


# Create your models here.

TIPOS_ACTIVIDADES = (
    ('Charla', 'Charla'),
    ('Recolección', 'Recolección'),
    ('Reforestación', 'Reforestación'),
    ('Taller', 'Taller'),
)

class Actividad(models.Model):
    tipo = models.CharField(max_length=20, choices=TIPOS_ACTIVIDADES)
    titulo = models.CharField(max_length=50)
    fecha = models.DateField()
    hora = models.TimeField()
    duracion = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    comuna = models.CharField(max_length=20)
    region = models.CharField(max_length=20)
    direccion = models.CharField(max_length=150)
    foto = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.titulo


class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    run = models.CharField(max_length=15)
    nacimiento = models.DateField()
    telefono = models.IntegerField()
    comuna = models.CharField(max_length=20)
    region = models.CharField(max_length=20)
    actividad = models.ManyToManyField(Actividad, null=True, blank=True)

    def __str__(self):
        return self.run