from django import forms
from django.forms import ModelForm
from .models import Actividad, Perfil


class ActividadForm(forms.ModelForm):
    class Meta:
        model = Actividad
        fields = ['tipo','titulo','fecha','hora','duracion','comuna','region','direccion','foto','user']

        exclude = ('user',)
        widgets = {
            'fecha':forms.SelectDateWidget()
        }
