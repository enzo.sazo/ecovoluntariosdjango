
$("#formulario").validate({

    rules: {
        "txtEmail": {
            required: true,
            email: true
        },
        "txtRun":{
            required: true,
            validRut: true
        },
        "txtNombre":{
            required: true,
            lettersonly: true
        },
        "txtApellido":{
            required: true,
            lettersonly: true
        },
        "dpFecha":{
            required: true,
            max: "2000-12-31"
        },
        "txtTelefono":{
            required: true,
            number: true,
            ComenzarEnNueve: true
        },
        "regiones":{
            required: true
        },
        "ciudades":{
            required: true
        },
        "actividades": {
            required: true
        }
    },

    messages: {

        "txtEmail": {
            required: "Ingrese su email",
            email: "Formato incorrecto"
        },
        "txtRun": {
            required: "Ingrese su rut",
            validRut: "Debes ingresar un rut válido"
        },
        "txtNombre":{
            required: "Ingrese su nombre",
            lettersonly: "Solo letras"
        },
        "txtApellido":{
            required: "Ingrese su apellido",
            lettersonly: "Solo letras"
        },
        "dpFecha":{
            required: "Ingrese su fecha de nacimiento",
            max: "Debe ser inferior al año 2001"
        },
        "txtTelefono":{
            required: "Ingrese su teléfono",
            number: "Ingrese solo números",
            ComenzarEnNueve: "El numero debe comenzar en 9"
        },
        "regiones":{
            required: "Seleccione una región"
        },
        "ciudades":{
            required: "Seleccione una comuna"
        },
        "actividades": {
            required: "Debe seleccionar al menos una actividad"
        },
    },
    
    errorPlacement: function(error, element) {
        if (element.attr("name") == "actividades") {
            error.insertAfter("#checkboxGroup");
        } else {
            error.insertAfter(element)
        }
    }
}
)

/* Cambio de color en los campos */
$(document).ready(function(){
    $("input").focus(function(){
      $(this).css("background-color", "rgb(238, 238, 238)");
    });
    $("input").blur(function(){
      $(this).css("background-color", "white");
    });
});


/* Validacion el telefono comienza en 9 */
$.validator.addMethod("ComenzarEnNueve", function(value, element) {
    return /^9/.test(value);
}, "input debe comenzar en 9");


/* RUT */
function checkRut(rut) {
    rut = String(rut);
    var valor = rut.replace(".", "").replace(".", "");
    valor = valor.replace("-", "");
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();
    rut = cuerpo + "-" + dv;
    if (cuerpo.length < 7) {
      return false;
    }
    suma = 0;
    multiplo = 2;
    for (i = 1; i <= cuerpo.length; i++) {
      index = multiplo * valor.charAt(cuerpo.length - i);
      suma = suma + index;
      if (multiplo < 7) {
        multiplo = multiplo + 1;
      } else {
        multiplo = 2;
      }
    }
    dvEsperado = 11 - suma % 11;
    dv = dv == "K" ? 10 : dv;
    dv = dv == 0 ? 11 : dv;
    if (dvEsperado != dv) {
      return false;
    }
    return true;
} 

$.validator.addMethod("validRut", function(value, element) {
    return checkRut(value);
    }, "Debes ingresar un rut válido"
);



//Regiones
var opt_1 = new Array ("","Arica", "Camarones", "Putre", "General Lagos");
var opt_2 = new Array ("","Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica");
var opt_3 = new Array ("","Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena");
var opt_4 = new Array ("","Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco");
var opt_5 = new Array ("","La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado");
var opt_6 = new Array ("","Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana");
var opt_7 = new Array ("","Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz");
var opt_8 = new Array ("","Talca", "ConsVtución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "ReVro", "San Javier", "Villa Alegre", "Yerbas Buenas");
var opt_9 = new Array ("","Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay");
var opt_10 = new Array ("","Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria");
var opt_11 = new Array ("","Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno");
var opt_12 = new Array ("","Puerto Montt", "Calbuco", "Cochamó", "Fresia", "FruVllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena");
var opt_13 = new Array ("","Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez");
var opt_14 = new Array ("","Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "AntárVca", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine");
var opt_15 = new Array ("","Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "TilVl", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor");


function cambia(){
    var regiones;
    regiones = document.formulario.regiones[document.formulario.regiones.selectedIndex].value;
    if(regiones!=0){
        mis_opts=eval("opt_" + regiones);
        num_opts=mis_opts.length;
        document.formulario.ciudades.length = num_opts;
        document.formulario.ciudades.options[0].index=0;
        for(i=1; i<num_opts; i++){
            document.formulario.ciudades.options[i].value=mis_opts[i];
            document.formulario.ciudades.options[i].text=mis_opts[i];
        }
        
    }else{
        document.formulario.ciudades.length = 1;
        document.formulario.ciudades.options[0].value="-";
        document.formulario.ciudades.options[0].text="-";
            
    }
    document.formulario.ciudades.options[0].selected = true;
}



