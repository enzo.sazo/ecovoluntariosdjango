from django.apps import AppConfig


class AppautenticacionConfig(AppConfig):
    name = 'appAutenticacion'
