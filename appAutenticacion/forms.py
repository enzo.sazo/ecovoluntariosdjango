from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from appEco.models import Perfil

class CustomUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', "last_name", "email", "username", "password1", "password2"]

class PerfilForm(ModelForm):
    class Meta:
        model = Perfil
        fields = ['run', 'nacimiento','telefono','comuna','region']
        
        widgets = {
            'nacimiento':forms.SelectDateWidget(years=range(1900,2019))
        }