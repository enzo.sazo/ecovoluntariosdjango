from django.shortcuts import render, redirect
from .forms import CustomUserForm, PerfilForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User, Group


# Create your views here.

def registro_usuario(request):
    data = {
        'form':CustomUserForm(),
        'perfil':PerfilForm()
    }
    if request.method == "POST":
        formulario = CustomUserForm(request.POST)
        perfil = PerfilForm(request.POST)
        if formulario.is_valid() and perfil.is_valid():
            new_user = formulario.save()
            perfil_user = perfil.save(commit=False)
            perfil_user.user = new_user
            perfil_user.save()
            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)

            # Esto permite que los usuarios al registrarse, sean de tipo cliente
            user.groups.add(Group.objects.get(name='Cliente'))
            return redirect( to='home')

    return render(request, 'registration/registrar.html', data)